﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleSorter.UnitTests
{
    [TestFixture]
    class TriangleTests
    {
        [TestCase(3,4,5)]
        [TestCase(3.5, 4.5, 5.5)]
        public void TellType_AllSidesDifferentValid_ShouldReturnScalene(double side1,double side2,double side3)
        {
            Triangle tri = new Triangle();
            string expected = "Scalene";

            string actual = tri.TellType(side1,side2,side3);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(3, 3, 5)]
        [TestCase(7.5, 7.5, 5.5)]
        public void TellType_TwoSidesSameValid_ShouldReturnIsosceles(double side1, double side2, double side3)
        {
            Triangle tri = new Triangle();
            string expected = "Isosceles";

            string actual = tri.TellType(side1, side2, side3);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(5, 5, 5)]
        [TestCase(8.5, 8.5, 8.5)]
        public void TellType_AllSidesSameValid_ShouldReturnEquilateral(double side1, double side2, double side3)
        {
            Triangle tri = new Triangle();
            string expected = "Equilateral";

            string actual = tri.TellType(side1, side2, side3);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(2,2,4)]
        [TestCase(2, 1, 4)]
        [TestCase(2.5, 1, 4)]
        public void TellType_SumofTwoSidesIsNotBiggerThanThird_ShouldReturnNotTriangle(double side1, double side2, double side3)
        {
            Triangle tri = new Triangle();
            string expected = "There is no such triangle";

            string actual = tri.TellType(side1, side2, side3);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(0,2,3)]
        [TestCase(5, 0, 3)]
        [TestCase(7, 2, 0)]
        public void TellType_OneSideisZero_ShouldReturnNoSideCanBeZero(double side1, double side2, double side3)
        {
            
            Triangle tri = new Triangle();
            string expected = "No side of a triangle can be zero";

            string actual = tri.TellType(side1, side2, side3);

            Assert.AreEqual(expected, actual);
        }
    }
}
