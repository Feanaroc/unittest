﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyzerLibrary
{
    public class LogAnalyzer : ILogAnalyzer
    {
        public bool LastFileNameWasValid { get; set; }
        IFileExtensionManager _mng;

        public LogAnalyzer() { }
        public LogAnalyzer(IFileExtensionManager mng)
        {
            _mng = mng;
        }


        public bool IsValidLogFileName(string fileName)
        {
            if (fileName == "")
            {
                throw new ArgumentException("File Name has to be provided");
            }

            LastFileNameWasValid = false;
            if (!fileName.EndsWith(".SLF", StringComparison.CurrentCultureIgnoreCase))
            {
                return false;
            }
            LastFileNameWasValid = true;
            return true;
        }

        public bool IsValidLogFileName_Stub(string fileName)
        {
            
            _mng = new FileExtensionManager();
            return _mng.IsValid(fileName);
        }
    }


}
