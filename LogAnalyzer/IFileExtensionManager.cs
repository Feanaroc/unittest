﻿namespace LogAnalyzerLibrary
{
    public interface IFileExtensionManager
    {
        bool IsValid(string fileName);
    }

    public class FileExtensionManager : IFileExtensionManager
    {
        public bool IsValid(string fileName)
        {
            return false;
        }
    }

    public class AlwaysTrueFakeFileExtension : IFileExtensionManager
    {
        public bool IsValid(string fileName)
        {
            return true;
        }
    }

    public class AlwaysFalseFakeFileExtensionStub : IFileExtensionManager
    {
        public bool IsValid(string fileName)
        {
            return false;
        }
    }

    public class FakeFileExtension : IFileExtensionManager
    {
        public bool WillbeValid = false;

        public bool IsValid(string fileName)
        {
            return WillbeValid;
        }
    }
}