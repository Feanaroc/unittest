﻿using BuzzFizzLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuzzFizz
{
    class Program
    {
        static void Main(string[] args)
        {
            BuzzFizzer product = new BuzzFizzer(); 
            for (int i = 1; i <= 100; i++)
            {
                Console.WriteLine(product.Return(i));
            }
            Console.ReadKey();
        }
    }
}
