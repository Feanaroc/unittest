﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleSorter
{
    public class Triangle
    {
        public double side1 { get; set; }
        public double side2 { get; set; }
        public double side3 { get; set; }

        public string TellType(double side1, double side2, double side3)
        {
            
            string result = "";
            if (side1 == 0 || side2 == 0 || side3 == 0)
            {
                result += "No side of a triangle can be zero";
            }
            else if (side1+side2 <= side3 || side2 + side3 <= side1 || side3 + side1 <= side2 )
            {
                result += "There is no such triangle";
            }
            else if (side1 != side2 && side2 != side3 && side1 !=side3 )
            {
                result += "Scalene";
            }
            else if ((side1 == side2 && side3 != side2)|| (side2 == side3 && side1 != side2) || (side1 == side3 && side3 != side2))
            {
                result += "Isosceles";
            }
            else if (side1 == side2 && side2 == side3 )
            {
                result += "Equilateral";
            }
            return result;
        }
    }
}
