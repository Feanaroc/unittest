﻿using ApprovalTests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApprovalTests.Reporters;

namespace BuzzFizzLibrary.Tests
{
    [TestFixture]
    public class BuzzFizzerTests
    {
        //[Test]
        //public void SampleTest()
        //{
        //    Assert.Pass();
        //}

        [Test]
        [UseReporter(typeof(DiffReporter))]
        public void Return_DefaultValue_ShouldGiveValue([Values(7)]int input)
        {
            string expected = input.ToString();
            BuzzFizzer sut = new BuzzFizzer();
            string actual = sut.Return(input);
            Assert.AreEqual(expected, actual);
            Approvals.Verify(actual);
        }

        [Test]
        public void Return_ValueIsMultipleOf3_ShouldGiveFizz([Values(3, 6, 9, 12)]int input)
        {
            string expected = "Buzz";
            BuzzFizzer sut = new BuzzFizzer();
            string actual = sut.Return(input);
            Assert.AreEqual(expected,actual);
        }

        [Test]
        [Category("NormalTests")]
        public void Return_ValueIsMultipleOf5_ShouldGiveBuzz([Values(5, 10, 20)]int input)
        {
            string expected = "Fizz";
            BuzzFizzer sut = new BuzzFizzer();
            string actual = sut.Return(input);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Return_ValueIsMultipleOf5and3_ShouldGiveFizzBuzz([Values(15,0)]int input)
        {
            string expected = "BuzzFizz";
            BuzzFizzer sut = new BuzzFizzer();
            string actual = sut.Return(input);
            Assert.AreEqual(expected, actual);
        }

    }
}
