﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemCalculatorLibrary
{
    public class MemCalculator
    {
        private int sum = 0;

        public void Add(int num)
        {
            sum += num;
        }

        public int Sum()
        {
            int temp = sum;
            sum = 0;
            return temp;
        }
    }
}
