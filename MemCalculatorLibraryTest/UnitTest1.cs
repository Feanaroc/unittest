﻿using System;
using NUnit.Framework;

namespace MemCalculatorLibrary.Tests
{
    [TestFixture]
    public class MemCalculatorTests
    {
        private static MemCalculator MakeCalc()
        {
            return new MemCalculator();
        }

        [Test]
        public void SumByDefault_ReturnsZero()
        {
            MemCalculator mem = MakeCalc();
            int expected = 0;

            Assert.AreEqual(expected, mem.Sum());
        }

        [Test]
        public void AddWhenCalled_ChangesSum()
        {
            MemCalculator mem = new MemCalculator();
            int expected = 1;

            mem.Add(1);

            Assert.AreEqual(expected, mem.Sum());
        }
    }
}
