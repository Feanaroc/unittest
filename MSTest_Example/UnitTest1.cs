﻿using System;
using BuzzFizzLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace MSTest_Example
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Return_DefaultValue_ShouldGiveValue()
        {
            int input = 1;
            string expected = input.ToString();
            BuzzFizzer sut = new BuzzFizzer();
            string actual = sut.Return(input);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Return_ValueIsMultipleOf3_ShouldGiveFizz()
        {
            int input = 3;
            string expected = "Buzz";
            BuzzFizzer sut = new BuzzFizzer();
            string actual = sut.Return(input);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        
        public void Return_ValueIsMultipleOf5_ShouldGiveBuzz()
        {
            int input = 5;
            string expected = "Fizz";
            BuzzFizzer sut = new BuzzFizzer();
            string actual = sut.Return(input);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Return_ValueIsMultipleOf5and3_ShouldGiveFizzBuzz()
        {
            int input = 15;
            string expected = "BuzzFizz";
            BuzzFizzer sut = new BuzzFizzer();
            string actual = sut.Return(input);
            Assert.AreEqual(expected, actual);
        }

    }
}
