﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyzerLibrary.Tests
{
    [TestFixture]
    class IsValidLogFileNameTest
    {
        [TestCase("asd.SLV")]
        [TestCase("asd.fls")]
        public void IsValidLogFileName_DoesntEndWithSLF_ReturnsFalse([Values("asd.SLV","ryr.slv")] string fileName)
        {
            LogAnalyzer sut = new LogAnalyzer();
            bool expected = false;

            bool actual = sut.IsValidLogFileName(fileName);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("asd.SLF")]
        [TestCase("asd.slf")]
        public void IsValidLogFileName_EndsWithSLF_ReturnsTrue(string fileName)
        {
            LogAnalyzer sut = new LogAnalyzer();
            bool expected = true;

            bool actual = sut.IsValidLogFileName(fileName);
            Assert.AreEqual(expected, actual);

        }

        [TestCase("asd.SLF")]
        [TestCase("asd.slf")]
        public void IsValidLogFileName_LastNameWasValid_ReturnsTrue(string fileName)
        {
            LogAnalyzer sut = new LogAnalyzer();
            bool expected = true;

            sut.IsValidLogFileName(fileName);
            bool actual = sut.LastFileNameWasValid;

            Assert.AreEqual(expected, actual);
        }

        [TestCase("asd.dse")]
        [TestCase("asd.lfs")]
        public void IsValidLogFileName_LastNameWasNotValid_ReturnsFalse(string fileName)
        {
            LogAnalyzer sut = new LogAnalyzer();
            bool expected = false;

            sut.IsValidLogFileName(fileName);
            bool actual = sut.LastFileNameWasValid;

            Assert.AreEqual(expected, actual);
        }

        
        [TestCase("")]
        public void IsValidLogFileName_NoFileName_ShouldThrowException(string fileName)
        {
            LogAnalyzer sut = new LogAnalyzer();

            var ex = Assert.Catch<Exception>(() => sut.IsValidLogFileName(""));

            StringAssert.Contains("File Name has to be provided", ex.Message);
        }

        [TestCase("")]
        public void IsValidLogFileName_Stub(string fileName)
        {
            FakeFileExtension mng = new FakeFileExtension();
            mng.WillbeValid = true;
            
            LogAnalyzer sut = new LogAnalyzer(mng);

            sut.IsValidLogFileName_Stub(fileName);
            
        }

        [TestCase("asd.sre")]
        public void IsValidLogFileName_Moq(string fileName)
        {
            var mockanalyzer = new Mock<ILogAnalyzer>();
            mockanalyzer.Setup(x => x.LastFileNameWasValid).Verifiable();
            mockanalyzer.Setup(x => x.LastFileNameWasValid).Returns(true);

            bool expected = true;

            mockanalyzer.Object.IsValidLogFileName(fileName);
            bool actual = mockanalyzer.Object.LastFileNameWasValid;

            mockanalyzer.Verify(x => x.IsValidLogFileName(fileName), Times.AtLeast(1));
            Assert.AreEqual(expected, actual);

        }
    }
}
