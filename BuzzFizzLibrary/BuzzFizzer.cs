﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuzzFizzLibrary
{
    public class BuzzFizzer
    {
        public string Return(int i)
        {
            string result = "";
            if (i % 3 == 0)
            {
                result += "Buzz";
            }
            if (i % 5 == 0)
            {
                result += "Fizz";
            }
            if (result =="")
            {
                result = i.ToString();
            }
            return result;
        }
    }
}
